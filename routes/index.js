const { Router } = require('express');
const employeeController = require('../controllers/employeeController');
const teamController = require('../controllers/teamController');
const router = Router();

//Employee routes
router.post('/employee', employeeController.createEmployee);
router.get('/employees', employeeController.getAllEmployee);
router.get('/employees/:id', employeeController.getEmployeeById);
router.put('/employees/:id', employeeController.updateEmployee);
router.delete('/employees/:id', employeeController.deleteEmployee);

//Team routes
router.post('/team', teamController.createTeam);
router.get('/teams', teamController.getAllTeam);
router.get('/teams/:id', teamController.getTeamById);
router.put('/teams/:id', teamController.updateTeam);
router.delete('/teams/:id', teamController.deleteTeam);

module.exports = router;
