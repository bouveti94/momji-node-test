const  { Employee, Team } = require('../models');

const createEmployee = async (req, res) => {
    try {
        const employee = await Employee.create(req.body);
        return res.status(201).json({
            employee,
        });
    } catch (error) {
        return res.status(500).json({ error: error.message })
    }
};

const getAllEmployee = async (req, res) => {
    try {
        const employees = await Employee.findAll({
            include: [
                {
                    model: Team
                }
            ]
        });
        return res.status(200).json({ employees });
    } catch (error) {
        return res.status(500).send(error.message);
    }
};

const getEmployeeById = async (req, res) => {
    try {
        const { id } = req.params;
        const employee = await Employee.findOne({
            where: { id: id },
            include: [
                {
                    model: Team
                }
            ]
        });
        if (employee) {
            return res.status(200).json({ employee });
        }
        return res.status(404).send('Employee with the specified ID does not exists');
    } catch (error) {
        return res.status(500).send(error.message);
    }
};

const updateEmployee = async (req, res) => {
    try {
        const { id } = req.params;
        const [updated] = await Employee.update(req.body, {
            where: { id: id }
        });
        if (updated) {
            const updateEmployee = await Employee.findOne({ where: { id: id } });
            return res.status(200).json({ employee: updateEmployee });
        }
        throw new Error('Employee not found');
    } catch (error) {
        return res.status(500).send(error.message);
    }
};

const deleteEmployee = async (req, res) => {
    try {
        const { id } = req.params;
        const deleted = await Employee.destroy({
            where: { id: id }
        });
        if (deleted) {
            return res.status(204).send("Employee deleted");
        }
        throw new Error("Employee not found");
    } catch (error) {
        return res.status(500).send(error.message);
    }
};

module.exports = {
    createEmployee,
    getAllEmployee,
    getEmployeeById,
    updateEmployee,
    deleteEmployee
};
