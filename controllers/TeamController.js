const { Team } = require('../models');

const createTeam = async (req, res) => {
    try {
        const team = await Team.create(req.body);
        return res.status(201).json({
            team,
        });
    } catch (error) {
        return res.status(500).json({ error: error.message })
    }
};

const getAllTeam = async (req, res) => {
    try {
        const teams = await Team.findAll({
            include: [
                {
                    model: Team
                }
            ]
        });
        return res.status(200).json({ teams });
    } catch (error) {
        return res.status(500).send(error.message);
    }
};

const getTeamById = async (req, res) => {
    try {
        const { id } = req.params;
        const team = await Team.findOne({
            where: { id: id },
            include: [
                {
                    model: Team
                }
            ]
        });
        if (team) {
            return res.status(200).json({ team });
        }
        return res.status(404).send('Team with the specified ID does not exists');
    } catch (error) {
        return res.status(500).send(error.message);
    }
};

const updateTeam = async (req, res) => {
    try {
        const { id } = req.params;
        const [updated] = await Team.update(req.body, {
            where: { id: id }
        });
        if (updated) {
            const updateTeam = await Team.findOne({ where: { id: id } });
            return res.status(200).json({ team: updateTeam });
        }
        throw new Error('Team not found');
    } catch (error) {
        return res.status(500).send(error.message);
    }
};

const deleteTeam = async (req, res) => {
    try {
        const { id } = req.params;
        const deleted = await Team.destroy({
            where: { id: id }
        });
        if (deleted) {
            return res.status(204).send("Team deleted");
        }
        throw new Error("Team not found");
    } catch (error) {
        return res.status(500).send(error.message);
    }
};

module.exports = {
    createTeam,
    getAllTeam,
    getTeamById,
    updateTeam,
    deleteTeam
};
